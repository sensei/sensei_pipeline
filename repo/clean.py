#!/usr/bin/env python2
from __future__ import print_function
import os, sys

dirname = os.path.dirname(__file__) or '.'
sys.path.append(dirname + '/..')

from util import repository


class AnnotationCleaner(repository.AnnotationGenerator):
    def __init__(self, presence_feature, annotation_set):
        self.feature = presence_feature
        self.annotation = annotation_set
        query = '_PRESENT_=%s&_MAX_=20' % presence_feature
        super(AnnotationCleaner, self).__init__(query)

    def process_document(self, client, document):
        doc_id = int(document['id'])
        client.delete_annotation_set(doc_id, self.annotation)
        client.delete_feature(doc_id, self.feature)
        print(doc_id)



if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description='Clean repository from annotation set', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--host', type=str, help='repository host', default='localhost')
    parser.add_argument('--port', type=int, help='repository port', default=8080)
    parser.add_argument('--presence_feature', type=str, help='feature to select documents (i.e. "AMU_hasTest")', required=True)
    parser.add_argument('--annotation_set', type=str, help='annotation set to remove (i.e. "AMU_Test")', required=True)
    args = parser.parse_args()

    client = repository.Client(host=args.host, port=args.port)
    cleaner = AnnotationCleaner(args.presence_feature, args.annotation_set)
    cleaner.run(client)
