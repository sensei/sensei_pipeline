#!/usr/bin/env python2
from __future__ import print_function
import os, sys
import subprocess
import json

dirname = os.path.dirname(__file__) or '.'
sys.path.append(dirname + '/..')

from util import repository

pipe = None
def run_command(directory, command, text, persistent):
    global pipe
    if not persistent or pipe is None:
        pipe = subprocess.Popen(command, shell=True, cwd=directory, stdout=subprocess.PIPE, stdin=subprocess.PIPE, stderr=subprocess.STDOUT, bufsize=0)    
    if persistent:
        if not text.endswith('\n'):
            text += '\n'
        pipe.stdin.write(text)
        pipe.stdin.flush()
        result = pipe.stdout.readline()
    else:
        result = pipe.communicate(input=text)[0]
    return result


class GenericAnnotator(repository.AnnotationGenerator):
    def __init__(self, mark_feature, annotation_set, document_query, command, directory, persistent):
        if document_query is None:
            document_query = mark_feature
        query = '_MISSING_=%s&_MAX_=20' % document_query
        super(GenericAnnotator, self).__init__(query)
        self.mark_feature = mark_feature
        self.annotation_set = annotation_set
        self.command = command
        self.directory = directory
        self.persistent = persistent

    def process_document(self, client, document):
        content = document['content']
        doc_id = int(document['id'])
        print(doc_id)
        annotation = run_command(self.directory, self.command, json.dumps(document), self.persistent)
        client.put_annotation_set(doc_id, self.annotation_set, json.loads(annotation))
        client.put_features(doc_id, {self.mark_feature: True})


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description='Generic repository annotator', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--host', type=str, help='repository host', default='localhost')
    parser.add_argument('--port', type=int, help='repository port', default=8080)
    parser.add_argument('--command', type=str, help='command to run', default='pwd')
    parser.add_argument('--directory', type=str, help='directory where to run command', default='.')
    parser.add_argument('--persistent', type=str, help='command is run once, and fed line by line', default=False)
    parser.add_argument('--mark_feature', type=str, help='feature to mark documents (i.e. "AMU_hasTest")', required=True)
    parser.add_argument('--annotation_set', type=str, help='annotation set name (i.e. "AMU_Test")', required=True)
    parser.add_argument('--query', type=str, help='document query (feature1=value1&feature2=value2...), defaults to the mark_feature')
    args = parser.parse_args()

    client = repository.Client(host=args.host, port=args.port)
    annotator = GenericAnnotator(args.mark_feature, args.annotation_set, args.query, args.command, args.directory, args.persistent)
    annotator.run(client)
