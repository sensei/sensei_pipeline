#!/usr/bin/env python2
from __future__ import print_function
import os, sys

dirname = os.path.dirname(__file__) or '.'
sys.path.append(dirname + '/..')

from util import repository
from core import test


class TestAnnotator(repository.AnnotationGenerator):
    def __init__(self):
        query = '_MISSING_=AMU_hasTest&_MAX_=20'
        super(TestAnnotator, self).__init__(query)
        self.tester = test.Test()

    def process_document(self, client, document):
        content = document['content']
        doc_id = int(document['id'])
        print(doc_id)
        annotation = self.tester.process(content)
        client.put_annotation_set(doc_id, 'AMU_Test', [{'type': 'test', 'features': {'reversed': annotation}, 'start': 0, 'end': len(content)}])
        client.put_features(doc_id, {'AMU_hasTest': True})


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description='Repository test annotator', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--host', type=str, help='repository host', default='localhost')
    parser.add_argument('--port', type=int, help='repository port', default=8080)
    args = parser.parse_args()

    client = repository.Client(host=args.host, port=args.port)
    tester = TestAnnotator()
    tester.run(client)
