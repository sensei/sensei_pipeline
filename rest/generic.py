#!/usr/bin/env python2
import bottle
import json
import sys
import os
import urllib
import subprocess

# Generic REST service
# Supports /name/<input> GET queries, as well as /name POST queries with input passed as POST argument.
# The command is either run for every query, or run in the background with --persistent True.
# The input is sent as stdin, and the output is read from stdout.
# In persistent mode, each input / output should be one liners.
# 
# Example use:
# python2 rest/generic.py --port 1234 --command "ls"  --directory ..
# python2 rest/generic.py --port 1234 --command "awk '{x+=1;print x,\$0;fflush()}'" --persistent True

pipe = None
def run_command(directory, command, text, persistent):
    global pipe
    if not persistent or pipe is None:
        pipe = subprocess.Popen(command, shell=True, cwd=directory, stdout=subprocess.PIPE, stdin=subprocess.PIPE, stderr=subprocess.STDOUT, bufsize=0)    
    if persistent:
        if not text.endswith('\n'):
            text += '\n'
        pipe.stdin.write(text)
        pipe.stdin.flush()
        result = pipe.stdout.readline()
    else:
        result = pipe.communicate(input=text)[0]
    return result

def setup_routes(name, command, directory, persistent):
    @bottle.get('/')
    def intex():
        return '<h2>SENSEI REST service: %s (command="%s", directory="%s", persistent="%s")</h2>' % (name, command, directory, persistent) \
             + '<ul>' \
             + '<li>GET /%s/&lt;input&gt; (parse input passed in url)</li>' % name \
             + '<li>POST /%s (process input passed in POST "input" field)</li>' % name \
             + '</ul>'

    @bottle.get('/%s/<text>' % name)
    def process_get(text):
        bottle.response.content_type = 'application/json'
        return run_command(directory, command, text, persistent)

    @bottle.post('/%s' % name)
    def process_post():
        global parser
        text = bottle.request.forms.get('text')
        bottle.response.content_type = 'application/json'
        return run_command(directory, command, text, persistent)

def run(port):
    bottle.run(host='0.0.0.0', port=port)

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description='Generic REST service', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--port', type=int, help='server port', default=1234)
    parser.add_argument('--name', type=str, help='service name', default='generic')
    parser.add_argument('--command', type=str, help='command to run', default='pwd')
    parser.add_argument('--directory', type=str, help='directory where to run command', default='.')
    parser.add_argument('--persistent', type=str, help='command is run once, and fed line by line', default=False)
    args = parser.parse_args()
    setup_routes(args.name, args.command, args.directory, args.persistent)
    run(args.port)

