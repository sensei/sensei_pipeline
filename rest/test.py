#!/usr/bin/env python2
import bottle
import json
import sys
import os
import urllib

# Simple example of rest service using the "test" core module

dirname = os.path.dirname(__file__) or '.'
sys.path.append('%s/..' % dirname)
from core import test

@bottle.get('/')
def intex():
    return '<h2>SENSEI REST service: test</h2>' \
         + '<ul>' \
         + '<li>GET /test/&lt;text&gt; (reverse text passed in url, utf8-encoded)</li>' \
         + '<li>POST /test (reverse text passed in POST "text" field, utf8-encoded)</li>' \
         + '</ul>' \
         + '<p>Example: <a href="/test/hello">/test/hello</a></p>\n' 

@bottle.get('/test/<text>')
def process_get(text):
    global tester
    bottle.response.content_type = 'application/json'
    return json.dumps(tester.process(text))

@bottle.post('/test')
def process_post():
    global tester
    text = bottle.request.forms.get('text')
    bottle.response.content_type = 'application/json'
    return json.dumps(tester.process(text))

def run(port):
    global tester
    tester = test.Test()
    bottle.run(host='0.0.0.0', port=port)

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description='REST tester', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--port', type=int, help='server port', default=1234)
    args = parser.parse_args()
    run(args.port)

